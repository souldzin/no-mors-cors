mod no_mors_cors;

use std::convert::Infallible;
use std::str::FromStr;

use clap::Parser;

use warp::path;
use warp::filters::path::FullPath;
use warp::http::{HeaderMap, Method};
use warp::hyper::body::Bytes;
use warp::{Filter};

use crate::no_mors_cors::proxy::process_and_proxy;
use crate::no_mors_cors::client::Request;
use crate::no_mors_cors::errors::Error;

/// Starts a server which will proxy based on the path
/// and add permissive CORS headers
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Port for the server to listen
    #[clap(short, long, default_value_t = 3030)]
    port: u16,
    
    /// Flag whether to serve over ssl or not
    #[clap(short, long)]
    ssl: bool,

    /// SSL: Path to cert file
    #[clap(short, long, default_value = "")]
    cert: String,

    /// SSL: Path to key file
    #[clap(short, long, default_value = "")]
    key: String
}

struct ProtocolParam(String);

impl FromStr for ProtocolParam {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "http" | "https" => Ok(ProtocolParam(s.to_string())),
            _                => Err(Error::ParseProxyRequest("Invalid protocol given in URL. Expected http or https.".to_string()))
        }
    }
}

fn build_request(
    protocol: ProtocolParam,
    host: String,
    uri: String,
    query: String,
    method: Method,
    headers: HeaderMap,
    body: Bytes
) -> Request {
    let full_uri = if query.is_empty() {
        uri
    } else {
        format!("{}?{}", uri, query)
    };

    return Request {
        protocol: protocol.0,
        host: host,
        uri: full_uri,
        body: body,
        headers: headers,
        method: method
    };
}

fn filter_for_query(
) -> impl Filter<Extract = (String,), Error = Infallible> + Clone {
    warp::query::raw()
        .or(
            warp::any().map(|| "".to_string())
        )
        .unify()
}

#[tokio::main]
async fn main() {
    println!("[no-mors-cors] Hello world!");

    let args = Args::parse();

    let app = path!(ProtocolParam / String / ..)
        .and(
            warp::path::full().map(|x: FullPath| {
                // Note: FullPath starts with a "/" which is why we have to +1
                let mut parts = x.as_str().splitn(4, '/');

                parts.nth(3).map_or_else(|| "", |x| x).to_string()
            })
        )
        .and(filter_for_query())
        .and(warp::method())
        .and(warp::header::headers_cloned())
        .and(warp::body::bytes())
        .map(build_request)
        .and_then(process_and_proxy)
        .boxed();

    print!("[no-mors-cors] Starting server on port {}... ", args.port);

    let server = warp::serve(app);

    if args.ssl {
        let task = server.tls()
            .cert_path(args.cert)
            .key_path(args.key)
            .run(([0, 0, 0, 0], args.port));

        println!("Done!");

        task.await;
    } else {
        let task = server.run(([0, 0, 0, 0], args.port));

        println!("Done!");

        task.await;
    }
}
