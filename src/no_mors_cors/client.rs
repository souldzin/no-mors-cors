// PLEASE NOTE ==============
// Most of this is lovingly borrowed from https://github.com/danielSanchezQ/warp-reverse-proxy
// ==========================

use http::Response;
use once_cell::sync::{Lazy, OnceCell};
use reqwest::redirect::Policy;
use unicase::Ascii;
use warp::{Rejection};
use warp::http::{HeaderMap, Method, HeaderValue};
use warp::hyper::body::Bytes;
use crate::no_mors_cors::errors::Error;

// types and structs ====================

pub struct Request {
    pub method: Method,
    pub protocol: String,
    pub host: String,
    pub uri: String,
    pub headers: HeaderMap,
    pub body: Bytes
}

// statics ==============================

static CLIENT: OnceCell<reqwest::Client> = OnceCell::new();

// public functions =====================

pub async fn request_and_forward_reply(
    request: Request,
) -> Result<Response<Bytes>, Rejection> {
    let req = to_client_request(request)
        .map_err(warp::reject::custom)?;

    let response = execute_request(req)
        .await
        .map_err(warp::reject::custom)?;

    response_to_reply(response)
        .await
        .map_err(warp::reject::custom)
}

// private functions ====================

fn to_client_request(request: Request) -> Result<reqwest::Request, Error> {
    let proxy_url = format!("{}://{}/{}", request.protocol, request.host, request.uri);

    let headers = remove_hop_headers(&request.headers);

    // DEBUG:
    // println!("url: {}", proxy_url);
    // println!("method: {}", request.method);
    // for h in request.headers.iter() {
    //     let val = h.1.to_str().unwrap();

    //     println!("{}: {}", h.0, val);
    // }

    CLIENT
        .get_or_init(default_reqwest_client)
        .request(request.method, &proxy_url)
        .headers(headers)
        .body(request.body)
        .build()
        .map_err(Error::Request)
}

/// Checker method to filter hop headers
///
/// Headers are checked using unicase to avoid case misfunctions
fn is_hop_header(header_name: &str) -> bool {
    static HOP_HEADERS: Lazy<Vec<Ascii<&'static str>>> = Lazy::new(|| {
        vec![
            Ascii::new("Connection"),
            Ascii::new("Keep-Alive"),
            Ascii::new("Proxy-Authenticate"),
            Ascii::new("Proxy-Authorization"),
            Ascii::new("Te"),
            Ascii::new("Trailers"),
            Ascii::new("Transfer-Encoding"),
            Ascii::new("Upgrade"),
        ]
    });

    HOP_HEADERS.iter().any(|h| h == &header_name)
}

fn remove_hop_headers(headers: &HeaderMap<HeaderValue>) -> HeaderMap<HeaderValue> {
    headers
        .iter()
        .filter_map(|(k, v)| {
            if !is_hop_header(k.as_str()) {
                Some((k.clone(), v.clone()))
            } else {
                None
            }
        })
        .collect()
}

async fn execute_request(request: reqwest::Request) -> Result<reqwest::Response, Error> {
    CLIENT
        .get_or_init(default_reqwest_client)
        .execute(request)
        .await
        .map_err(Error::Request)
}

async fn response_to_reply(
    response: reqwest::Response,
) -> Result<http::Response<Bytes>, Error> {
    let mut builder = http::Response::builder();

    for (k, v) in remove_hop_headers(response.headers()).iter() {
        builder = builder.header(k, v);
    }

    builder
        .status(response.status())
        .body(response.bytes().await.map_err(Error::Request)?)
        .map_err(Error::HTTP)
}

fn default_reqwest_client() -> reqwest::Client {
    reqwest::Client::builder()
        .redirect(Policy::none())
        .build()
        // we should panic here, it is enforce that the client is needed, and there is no error
        // handling possible on function call, better to stop execution.
        .expect("Default reqwest client couldn't build")
}
