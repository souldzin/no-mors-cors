use std::str::FromStr;

use warp::{Rejection};
use warp::http::{HeaderValue};
use warp::http::header::HeaderName;
use warp::hyper::body::Bytes;
use http::{Response, HeaderMap};
use crate::no_mors_cors::client::{Request, request_and_forward_reply};
use crate::no_mors_cors::errors::Error;

// public functions ==============================

pub async fn process_and_proxy(
    proxy_request: Request,
) -> Result<Response<Bytes>, Rejection> {
    let request = map_proxy_request_to_client_request(proxy_request)
        .map_err(warp::reject::custom)?;

    let response = request_and_forward_reply(request).await?;
        
    process_response(response).map_err(warp::reject::custom)
}

// private functions ==============================

fn process_response(
    mut response: Response<Bytes>
) -> Result<Response<Bytes>, Error> {
    let headers = response.headers_mut();

    replace_header(headers, "Access-Control-Allow-Origin", "*")?;
    replace_header(headers, "Access-Control-Allow-Methods", "*")?;
    replace_header(headers, "Access-Control-Allow-Headers", "*")?;

    Ok(response)
}


fn replace_header(
    headers: &mut HeaderMap,
    key: &'static str,
    value: &str,
) -> Result<(), Error> {
    headers.remove(key);

    let header_value = HeaderValue::from_str(value)
        .map_err(|_| Error::ParseProxyRequest(format!("Failed to create header value for {}", value)))?;

    let header_name = HeaderName::from_str(key)
        .map_err(|_| Error::ParseProxyRequest(format!("Failed to create header name for {}", value)))?;

    headers.append(header_name, header_value);

    Ok(())
}

fn map_proxy_request_to_client_request(
    mut req: Request
) -> Result<Request, Error> {
    replace_header(&mut req.headers, "host", &req.host)?;

    return Ok(req);
}
