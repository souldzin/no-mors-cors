# no-mors-cors

This is a web server that can be used to proxy and strip out CORS preventing headers.

WARNING: Not meant for any kind of production use!

## Usage

### How to make requests?

When requesting from a `no-mors-cors` server, include the protocol and host in your path. For example:

```shell
https://no-mors-cors.test:4040/https/gdk.test:3000/gitlab-org/gitlab/-/files/
```

### How to start server with Docker?

TBD

## Development

Here's some "How to's" when developing on this repo.

### How to run for development locally?

Make sure you have [`cargo` and rust installed](https://www.rust-lang.org/). Simply run:

```shell
cargo run
```

### How to build the docker image locally?

```shell
docker build -t no-mors-cors .
```
