FROM rust:1.60 as builder
WORKDIR /usr/src/no-mors-cors
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/no-mors-cors /usr/local/bin/no-mors-cors
RUN apt update && apt install openssl curl -y
CMD ["no-mors-cors"]
